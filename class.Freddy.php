<?php

/**
 * To whomever it may concern,
 *
 * Thank you for taking the time to consider me as a candidate for your job opening. This file serves as an alternative
 * to a standard, run of the mill resumé. I wrote this for sole purpose of demonstrating my creativity, experience and
 * abilities in a way beyond that of which a stale, static, and super duper boring PDF document can do.
 *
 * This document instead consists of novel code which either returns my CV, or which calculates your personal
 * "Freddyness" score – which I honestly hope, at least for your sake, will be low. Both cases serve output in JSON
 * format. At this point it is probably worth noting that I fully recognise the fact this code is:
 *
 *    1. horrifically over engineered;
 *    2. utterly ridiculous in every possible way.
 *
 * However, it is written strictly to achieve its purpose of impressing you and hopefully amusing you along the way. I,
 * for one, had an absolute blast writing and testing it.
 *
 * Please refer to my LinkedIn profile for a more traditional look at my skill set and for more detailed information
 * about my employment history, projects I have worked on in the past and projects which I am currently assigned to.
 * Naturally, if you require more information, it is freely available upon request.
 *
 * Thank you again for taking my application (pun intended) into consideration.
 *
 * Kind regards,
 *
 * Freddy de Noord
 *
 * @author Freddy de Noord <hi@freddydenoord.nl>
 * @link www.linkedin.com/in/freddydenoord LinkedIn
 * @link www.github.com/freddyamsterdam Github
 * @copyright Copyright (c) 2017, Freddy de Noord
 */

class Freddy
{

    private $allowedMethods = ['GET', 'POST'];


    private $points = 0;


    private $pointsAvailable = 0;


    private $score = 0;


    private $options = [
      'minimumScore' => 1,
      'maximumScore' => 10
    ];


    private $data;


    private $diff = [];


    private $freddy = [
      'personal' => [
        'information' => [
          'name' => "Freddy",
          'surname' => "de Noord",
          'birthday' => "21-12-1988",
          'birthplace' => "Arnhem, the Netherlands",
          'languages' => [
            [
              'language' => "Dutch",
              'proficiency' => "Native speaker"
            ],
            [
              'language' => "English",
              'proficiency' => "Native speaker"
            ],
            [
              'language' => "German",
              'proficiency' => "Can order a beer"
            ],
            [
              'language' => "French",
              'proficiency' => "Can order a cheese omelette"
            ],
            [
              'language' => 'Croatian',
              'proficiency' => [
                "Can order Rakija",
                "Able to communicate with dogs"
              ]
            ]
          ]
        ],
        'interests' => [
          "Collecting records",
          "Composing electronic music",
          "Olympic Weightlifting",
          "Overall fitness and health",
          "Food",
          "Craft beer",
          "Dog training"
        ]
      ],
      'education' => [
        [
          'institution' => 'SAE Institute',
          'location' => 'Oxford, United Kingdom',
          'level' => 'University',
          'course' => 'Audio Production',
          'degree' => 'Bachelor of Arts'
        ],
        [
          'institution' => 'Universiteit van Amsterdam',
          'location' => 'Amsterdam, the Netherlands',
          'level' => 'University',
          'course' => 'Artificial Intelligence',
          'degree' => 'n.a.'
        ]
      ],
      'experience' => [
        [
          'company' => "Freddy de Noord",
          'position' => "Full stack developer",
          'start date' => "November 2010",
          'end date' => "Present"
        ],
        [
          'company' => "Stumedia",
          'position' => "Full stack developer",
          'start date' => "October 2013",
          'end date' => "November 2015"
        ]
      ],
    ];


    private $derpMersergers = [
      'request method' => [
        'message' => "Eek! Please use supported HTTP method requests only.",
        'status' => 405
      ],
      'input data incorrect' => [
        'message' => "Yikes! Please use JSON format for your input data.",
        'status' => 400
      ],
      'input data missing' => [
        'message' => "Whoops! Please provide some input data.",
        'status' => 400
      ],
      'personal missing' => [
        'message' => "Haayyyoo! No personal info? That's kind of creepy.",
        'status' => 400
      ],
      'education missing' => [
        'message' => "No school? Hey, that's cool. Book smart isn't the only type of smart.",
        'status' => 400
      ],
      'experience missing' => [
        'message' => "No work experience? No worries. Everybody has to start somewhere!",
        'status' => 400
      ],
      'cloning not possible' => [
        'message' => "This can't be. Perfect cloning is impossible: https://youtu.be/owPC60Ue0BE.",
        'status' => 500
      ],
      'freddification not implemented' => [
        'message' => "Ermahgerd! Freddification is not supported yet.",
        'status' => 501
      ],
    ];


    function __construct(array $options = [], bool $debug = null)
    {

        try {

            if ($debug = true) {

                ini_set('display_errors', true);
                ini_set('display_startup_errors', true);
                error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

            }

            if ((isset($options['minimumScore']) && !is_numeric($options['minimumScore']))
                || (isset($options['maximumScore']) && !is_numeric($options['maximumScore']))) {

                $this->handleDerp($this->derpMersergers['score range not numeric']);

            }

            $this->options = array_merge($this->options, $options);

        } catch (Exception $e) {

            die($this->prepareResponse(['error' => $e->getMessage()], $e->getCode()));

        }

    }


    function __call(string $method, array $arguments = [])
    {

        try {

            if (method_exists($this, $method)) {

                $this->checkRequestMethod($_SERVER['REQUEST_METHOD']);
                $response = $this->$method(implode(',', $arguments));

                return $this->prepareResponse($response);

            }

        } catch (Exception $e) {

            die($this->prepareResponse(['error' => $e->getMessage()], $e->getCode()));

        }

    }


    private function handleDerp(array $derp)
    {

        throw new Exception ($derp['message'], $derp['status']);

    }


    private function checkRequestMethod(string $method)
    {

        if (!in_array($method, $this->allowedMethods)) {
            $this->handleDerp($this->derpMersergers['request method']);
        }

    }


    private function parseInputData(string $data)
    {

        $this->data = json_decode($data, true);

        if (!is_array($this->data)) {
            $this->handleDerp($this->derpMersergers['input data incorrect']);
        }

        if (empty($this->data)) {
            $this->handleDerp($this->derpMersergers['input data missing']);
        }

        if (!array_key_exists('personal', $this->data)) {
            $this->handleDerp($this->derpMersergers['personal missing']);
        }

        if (!array_key_exists('education', $this->data)) {
            $this->handleDerp($this->derpMersergers['personal missing']);
        }

        if (!array_key_exists('experience', $this->data)) {
            $this->handleDerp($this->derpMersergers['experience missing']);
        }

        return $this->data;

    }


    private function prepareResponse(array $response = [], int $status_code = null)
    {

        $status = $this->prepareStatus($status_code);
        $allowedMethods = implode(',', $this->allowedMethods);

        header("{$_SERVER['SERVER_PROTOCOL']} {$status}");
        header("Status: {$status}");
        header("Access-Control-Allow-Orgin: {$_SERVER['HTTP_ORIGIN']}");
        header("Access-Control-Allow-Methods: {$allowedMethods}");
        header("Access-Control-Allow-Credentials: true");
        header("Content-Type: application/json");

        return json_encode($response);

    }


    private function prepareStatus(int $status_code = null)
    {

        switch ($status_code) {
            case 201:
                $status = 'Created';
                break;

            case 304:
                $status = 'Not Modified';
                break;

            case 400:
                $status = 'Bad Request';
                break;

            case 401:
                $status = 'Unauthorised';
                break;

            case 403:
                $status = 'Forbidden';
                break;

            case 404:
                $status = 'Not Found';
                break;

            case 404:
                $status = 'Method Not Allowed';
                break;

            case 500:
                $status = 'Internal Server Error';
                break;

            case 501:
                $status = 'Not Implemented';
                break;

            default:
                $status_code = 200;
                $status = 'OK';
                break;
        }

        return "{$status_code} {$status}";

    }


    private function compareData(array $reference, array $input)
    {

        foreach ($input as $k => $v) {

            if (is_array($reference[$k]) && is_array($v)) {
                $this->compareData($reference[$k], $v);
            } elseif ($reference[$k] === $v) {
                $this->points++;
            } elseif (!array_key_exists($k, $reference)) {
                $this->diff[] = ['reference' => $reference[$k], 'input' => $v];
            }

        }

        foreach ($reference as $k => $v) {

            if (is_array($input[$k]) && is_array($v)) {
                $this->compareData($input[$k], $v);
            } elseif (!array_key_exists($k, $input) || $input[$k] !== $v) {
                $this->points--;
                $this->diff[] = ['reference' => $v, 'input' => $input[$k]];
            }

        }

    }


    private function getFreddy()
    {

        return $this->freddy;

    }


    private function calculateFreddyness(string $data = null, bool $freddify = null)
    {

        $this->data = $this->parseInputData($data);

        if ($this->freddy === $this->data) {
            $this->handleDerp($this->derpMersergers['cloning not possible']);
        }

        $this->compareData($this->freddy, $this->data);

        $this->pointsAvailable = $this->points + count($this->diff);
        $this->score = $this->options['minimumScore'] +
                      ($this->options['maximumScore'] / $this->pointsAvailable * $this->points);

        return [
          'score' => round($this->score, 1) . '/' . $this->options['maximumScore'],
          'todo' => $freddify ? $this->freddify($this->diff) : null
        ];

    }


    private function freddify(array $diff = [])
    {

        $this->handleDerp($this->derpMersergers['freddification not implemented']);

    }


}
