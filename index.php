<?php

require_once __DIR__ . "/class.Freddy.php";


$freddy = new Freddy(['minimumScore' => 0,'maximumScore' => 10]);


switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        if (isset($_POST['data'])) {
            echo $freddy->calculateFreddyness($_POST['data']);
        } else {
            $data = $freddy->getFreddy();
            echo $freddy->calculateFreddyness($data);
        }
        break;

    default:
        echo $freddy->getFreddy();
        break;
}
